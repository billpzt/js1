function conversorSegundos(segsTotal) {
    let horas = 0;
    let minutos = 0;
    let segundos = 0;

    /*Pega a quantidade de horas cheias*/
    horas = Math.floor(segsTotal / 3600);

    if (horas > 0) {
        /*Pega os segundos que sobraram quando tiramos as horas cheias*/
        segundos = segsTotal - (horas * 3600); 
    } else {
        segundos = segsTotal;
    }

    /*Pega a quantidade de minutos cheios*/
    minutos = Math.floor(segundos / 60);

    if (minutos > 0) {
        /*Pega os segundos que sobraram quando tiramos os minutos cheios*/
        segundos = segundos - (minutos * 60);
    }

    console.log(`${horas}:${minutos}:${segundos}`)
}