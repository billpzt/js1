/*([0,25], (25,50], (50,75], (75,100])

O símbolo ( representa "maior que". Por exemplo:
[0,25]  indica valores entre 0 e 25.0000, inclusive eles.
(25,50] indica valores maiores que 25 Ex: 25.00001 até o valor 50.0000000*/

function faixa(n) {
    if (n >= 0 && n <= 25) {
        console.log("0 - 25");
    } else if (n > 25 && n <= 50) {
        console.log("26 - 50");
    } else if (n > 50 && n <= 75) {
        console.log("51 - 75");
    } else if (n > 75 && n <= 100) {
        console.log("76 - 100");
    } else {
        console.log("Fora de intervalo");
    }       
}